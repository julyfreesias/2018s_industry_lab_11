package ictgradschool.industry.lab.swing.ex04;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import javax.swing.Timer;

/**
 * Displays an animated balloon.
 */
public class ExerciseFourPanel extends JPanel implements ActionListener, KeyListener {

    private  Balloon balloon;
    private  List<Balloon> balloons = new ArrayList<Balloon>(); //made a ArrayList to make many balloons
    private  JButton moveButton;
    private  Timer myTime;
    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseFourPanel() {
        setBackground(Color.white);

        this.balloon = new Balloon(30, 60);

        this.moveButton = new JButton("Move balloon");
        this.moveButton.addActionListener(this);
        this.add(moveButton);
        this.addKeyListener(this); //to get Keyboard input
        myTime=new Timer(100,this); //to set the Timer for moving automatically

    }

    /**
     * Moves the balloon and calls repaint() to tell Swing we need to redraw ourselves.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        balloon.move();
        for(Balloon present :balloons){
            present.move();
        }

        // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
        // events even after we've clicked the button.
        requestFocusInWindow();

        repaint();
    }

    /**
     * Draws any balloons we have inside this panel.
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        balloon.draw(g);
        for(Balloon present: balloons){ //bring the all the balloons from arrayList and draw it using for loop
            present.draw(g);
        }
        
        // Sets focus outside of actionPerformed so key presses work without pressing the button
        requestFocusInWindow();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

        if(!myTime.isRunning()){//when timer is not running (before press any key from Keyboard)
            myTime.start();    //then the balloon will move automatically
        }

        if (e.getKeyCode()==KeyEvent.VK_UP) {   //start with VK_ for certain keyboard type.
            balloon.setDirection(Direction.Up);  // up,down,left, right is for arrows key
        }else if (e.getKeyCode()==KeyEvent.VK_DOWN) { //other alphabet is the one corresponding to keyboard alphabet.
            balloon.setDirection(Direction.Down);
        } else if (e.getKeyCode()==KeyEvent.VK_LEFT) {
            balloon.setDirection(Direction.Left);
        } else if (e.getKeyCode()==KeyEvent.VK_RIGHT) {
            balloon.setDirection(Direction.Right);
        }else if (e.getKeyCode()==KeyEvent.VK_S){
            if(myTime.isRunning()){
                myTime.stop(); //when timer is running, if press S button, then stop the myTime.
            }
        }else if (e.getKeyCode()==KeyEvent.VK_R){ //if push R button on Keyboard,then balloons will be added in random position
            balloons.add(new Balloon((int)(Math.random()*800),(int)(Math.random()*800)));

        }






    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}