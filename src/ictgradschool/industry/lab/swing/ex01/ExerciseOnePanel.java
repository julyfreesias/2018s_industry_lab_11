package ictgradschool.industry.lab.swing.ex01;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple GUI app that does BMI calculations.
 */
public class ExerciseOnePanel extends JPanel implements ActionListener {

    // TODO Declare JTextFields and JButtons as instance variables here.
    private JTextField height;   //need to make instance variables rather than make it local variables
    private JTextField weight;  //in the ExerciseOnePanel() method.
    private JButton calBmiButton;        //so that we could use this for ActionPerformed()method.
    private JButton calHWButton;        //if you make this a local variables, then you can not apply the action activation
    private JTextField bmi;  //like button and textField from UserInput.
    private JTextField maxWeight;




    /**
     * Creates a new ExerciseOnePanel.
     */
    public ExerciseOnePanel() {

        setBackground(Color.white);

        // TODO Construct JTextFields and JButtons.
        // HINT: Declare them as instance variables so that other methods in this class (e.g. actionPerformed) can
        // also access them.
        JLabel label = new JLabel("Height in metres");
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(label);

        this.add(Box.createVerticalStrut(10));
        height = new JTextField(15);      ///simply increase the number of text you take.. so we can adjust the size of textbox.
        this.add(height);

        JLabel labe2 = new JLabel("Weight in Kilograms");
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(labe2);


        weight = new JTextField(15);
        this.add(weight);


        calBmiButton = new JButton("calculate BMI");
        this.add(calBmiButton);
        calBmiButton.addActionListener(this);

        JLabel labe3 = new JLabel("Your Body Mass Index(BMI)is:");
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(labe3);

        bmi = new JTextField(15);
        this.add(bmi);

        calHWButton = new JButton("Calculate Healthy Weight");
        this.add(calHWButton);
        calHWButton.addActionListener(this);

        JLabel labe4 = new JLabel("Maximum Healthy Weight for your Height:");
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(labe4);

        maxWeight = new JTextField(15);
        this.add(maxWeight);



        // TODO Declare and construct JLabels
        // Note: These ones don't need to be accessed anywhere else so it makes sense just to declare them here as
        // local variables, rather than instance variables.

        // TODO Add JLabels, JTextFields and JButtons to window.
        // Note: The default layout manager, FlowLayout, will be fine (but feel free to experiment with others if you want!!)

        // TODO Add Action Listeners for the JButtons

    }


    /**
     * When a button is clicked, this method should detect which button was clicked, and display either the BMI or the
     * maximum healthy weight, depending on which JButton was pressed.
     */
    public void actionPerformed(ActionEvent event) {
        double weightL = Double.parseDouble(weight.getText());
        double heightL = Double.parseDouble(height.getText());

        if(event.getSource()==calBmiButton)
        {
            double calbmi = weightL / (heightL * heightL);
            bmi.setText(roundTo2DecimalPlaces(calbmi)+"");
        }
        if(event.getSource()==calHWButton){

            double healthweight = 24.9 * heightL * heightL ;
            maxWeight.setText(roundTo2DecimalPlaces(healthweight)+"");
        }


        // TODO Implement this method.
        // Hint #1: event.getSource() will return the button which was pressed.
        // Hint #2: JTextField's getText() method will get the value in the text box, as a String.
        // Hint #3: JTextField's setText() method will allow you to pass it a String, which will be diaplayed in the text box.
    }


    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}