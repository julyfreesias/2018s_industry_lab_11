package ictgradschool.industry.lab.swing.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel implements ActionListener{

    /**
     * Creates a new ExerciseFivePanel.
     */
    private JTextField firstNum;
    private JTextField secondNum;
    private JButton addBtn;
    private JButton subtBtn;
    private JTextField result;



    public ExerciseTwoPanel() {
        setBackground(Color.white);

        firstNum = new JTextField(15);
        this.add(firstNum);

        secondNum = new JTextField(15);
        this.add(secondNum);

        addBtn = new JButton("Add");
        this.add( addBtn );
        addBtn.addActionListener(this);

        subtBtn = new JButton("Substract");
        this.add( subtBtn );
        subtBtn.addActionListener(this);

        JLabel labe = new JLabel("Result");
        labe.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(labe);

        result = new JTextField(15);
        this.add(result);



    }

    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        double firstNumL = Double.parseDouble(firstNum.getText());
        double  secondNumL =Double.parseDouble(secondNum.getText());

        if(e.getSource()==addBtn) {
            double calAdd = firstNumL + secondNumL;
            result.setText(roundTo2DecimalPlaces(calAdd)+"");
        }
        if(e.getSource()==subtBtn){
            double calSubt = firstNumL - secondNumL;
            result.setText(roundTo2DecimalPlaces(calSubt)+"");
        }

    }
}